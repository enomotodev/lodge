<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function post_detail()
    {
        return $this->hasOne('App\Models\PostDetail');
    }
}
