<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class DevelopServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $providers = [
        'Barryvdh\Debugbar\ServiceProvider',
        'Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider',
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->isLocal()) {
            $this->registerServiceProviders();
        }
    }

    /**
     * @return void
     */
    protected function registerServiceProviders()
    {
        foreach ($this->providers as $provider) {
            $this->app->register($provider);
        }
    }
}
