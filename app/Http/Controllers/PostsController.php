<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Post;

class PostsController extends Controller
{
    public function show($id)
    {
        $post = Post::findOrFail($id);

        return view('contents.posts.show', compact('post'));
    }

    public function create()
    {
        return view('contents.posts.create');
    }

    public function store()
    {
        $inputs = \Request::all();

        $post = new Post;
        $post->save();
        $post->post_detail()->create($inputs);

        \Session::flash('flash_message', 'Add Post.');

        return redirect()->route('index');
    }
}
