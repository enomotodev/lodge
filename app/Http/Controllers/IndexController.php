<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Post;

class IndexController extends Controller
{
    public function index()
    {
        $posts = Post::with('post_detail')->get();

        return view('contents.index.index', compact('posts'));
    }
}
