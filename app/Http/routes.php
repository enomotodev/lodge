<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'IndexController@index']);

    Route::get('posts/create', ['as' => 'posts.create', 'uses' => 'PostsController@create']);
    Route::post('posts/create', ['as' => 'posts.create', 'uses' => 'PostsController@store']);
    Route::get('posts/{id}', ['as' => 'posts.show', 'uses' => 'PostsController@show']);
});
