@extends('layouts.default')

@section('content')
    <h1>index</h1>

    {!! link_to(route('posts.create'), 'New', ['class' => 'btn btn-primary']) !!}

    @foreach ($posts as $post)
        <h2><a href="{{ route('posts.show', $post->id) }}">{{ $post->post_detail->title }}</a></h2>
    @endforeach
@endsection