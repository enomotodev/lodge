@extends('layouts.default')

@section('content')
    <h1>{{ $post->post_detail->title  }}</h1>

    {!! Markdown::convertToHtml($post->post_detail->body) !!}
@endsection